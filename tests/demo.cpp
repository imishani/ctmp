/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   actionSpace.hpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   5/1/23
*/


#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <ctmp/ctmpAction.h>
#include <manipulation_planning/common/utils.hpp>
#include <random>

int main(int argc, char **argv){
    ros::init(argc, argv, "ctmp_client_demo");
    // create the action client
    // true causes the client to spin its own thread
    actionlib::SimpleActionClient<ctmp::ctmpAction> ac1("manipulator_1/ctmp_action", true);
    actionlib::SimpleActionClient<ctmp::ctmpAction> ac2("manipulator_2/ctmp_action", true);
    actionlib::SimpleActionClient<ctmp::ctmpAction> ac3("manipulator_3/ctmp_action", true);

    ROS_INFO("Waiting for action server to start.");
    // wait for the action server to start
    ac1.waitForServer(); //will wait for infinite time
    ac2.waitForServer(); //will wait for infinite time
    ac3.waitForServer(); //will wait for infinite time
    ROS_INFO("Action server started, sending goal.");
    // send a goal to the action

    std::vector<double> goal_state (6, 0.0);
    // get a random goal state in the goal region
    std::vector<double> min_limits, max_limits;
    ros::param::get("/manipulator_1/regions/place_region/min_limits", min_limits);
    ros::param::get("/manipulator_1/regions/place_region/max_limits", max_limits);

    std::random_device rd;
    std::mt19937 gen(rd());
    // generate a random goal state
    for (int i = 0; i < 6; i++) {
        std::uniform_real_distribution<> dis(min_limits[i], max_limits[i]);
        goal_state[i] = dis(gen);
    }

    ctmp::ctmpGoal goal;
    geometry_msgs::Pose place_pose;
    goal.robot_name = "manipulator_1";
    goal.pick_object_name = "part_c2";
    place_pose.position.x = goal_state[0];
    place_pose.position.y = goal_state[1];
    place_pose.position.z = goal_state[2];

    ims::from_euler_zyx(goal_state[3], goal_state[4], goal_state[5], place_pose);
    goal.place_pose = place_pose;

    ac2.sendGoal(goal);;
    bool finished_before_timeout = ac2.waitForResult(ros::Duration(50.0));
    if (finished_before_timeout && ac2.getState() == actionlib::SimpleClientGoalState::SUCCEEDED){
        actionlib::SimpleClientGoalState state = ac2.getState();
        ROS_INFO("Action finished: %s",state.toString().c_str());
    }
    else
        ROS_INFO("Action did not finish before the time out.");

    //exit
    return 0;
}