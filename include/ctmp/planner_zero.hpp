/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   actionSpace.hpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   4/12/23
*/

#ifndef CTMP_PLANNER_ZERO_HPP
#define CTMP_PLANNER_ZERO_HPP

#include <ctmp/ctmp_action_space.hpp>

#include <search/planners/best_first_search.hpp>
#include <utility>
#include <unordered_set>


namespace ims {

    class plannerZero : public BestFirstSearch{
    private:

        /// @brief The search state.
        struct SearchState : public BestFirstSearch::SearchState {
            /// @brief The heuristic value
            double h = -1;
            /// @brief bool indicating whether the state is covered
            bool covered = false;
            bool covered_this = false;
            /// @brief bool indicating if greedy
            bool greedy {};
        };

        /// @brief The open list.
        using OpenList = ::smpl::IntrusiveHeap<SearchState, SearchStateCompare>;
        OpenList open_;

        std::vector<SearchState*> states_;

    public:

        /// @brief Get the state by id
        /// @param state_id The id of the state
        /// @return The state
        /// @note Use this function only if you are sure that the state exists
        auto getSearchState(int state_id) -> SearchState*;

        /// @brief Get the state by id or create a new one if it does not exist
        /// @param state_id The id of the state
        /// @return The state
        auto getOrCreateSearchState(int state_id) -> SearchState*;

        /// @brief Constructor
        /// @param params The parameters of the planner
        explicit plannerZero(const BestFirstSearchParams &params);

        void initializePlanner(const std::shared_ptr<ctmp_action_space>& actionSpacePtr,
                               const StateType & start, const StateType & goal) ;

        void reinitSearchState(int s_ind);

        bool is_state_covered(int id);

//        unsigned int compute_reachability(unsigned int r_max,
//                                 int attractor_state_id);
        double compute_reachability(double r_max,
                                          int attractor_state_id);


        void get_frontier_stateids(std::vector<int>& state_ids);

//        int search_for_valid_uncovered_states(
//                unsigned int r_max,
//                int iv_start_state_id);

        double search_for_valid_uncovered_states(
                double r_max,
                int iv_start_state_id);

        /// @brief Find a greedy path from goal state to attractor state and then return the reverse path.
        /// @param goal_state_id The goal state id.
        /// @param attr_state_id The attractor state id.
        /// @param path The path from goal to attractor state.
        /// @return True if a path was found, false otherwise.
        bool findGreedyPath(int goal_state_id, int attr_state_id, std::vector<int>& path);

        void resetPlanningData() override;

    protected:

        std::shared_ptr<ctmp_action_space> ctmp_action_space_;

        int call_number_;

        int iteration_;

        std::vector<int> succs_;
        std::vector<int> preds_;
        std::vector<double> costs_;

        // reachability
        int reachability_expansions_;
        int search_mode_;
        int best_state_;
        double h_max_;
        std::vector<SearchState*> h_max_states_;
        // object which contains all state index which where init during compute_reachability.
        // needs to be fat to look for a state
        std::unordered_set<int> state_initiated_;

    };
}


#endif //CTMP_PLANNER_ZERO_HPP
