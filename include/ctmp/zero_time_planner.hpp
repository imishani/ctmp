/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   actionSpace.hpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   4/12/23
*/

//#include "ctmp/ctmp_action_space.hpp"
#include <ctmp/ctmp_utils.hpp>
//#include <manipulation_planning/utils.hpp>
#include <ctmp/planner_zero.hpp>
#include <search/planners/wastar.hpp>

// search includes
#include <search/common/types.hpp>


#ifndef CTMP_ZERO_TIME_PLANNER_HPP
#define CTMP_ZERO_TIME_PLANNER_HPP


class ZeroTimePlanner {
public:

    /// \brief The class constructor
    /// TODO: Implement the constructor
    ZeroTimePlanner() = default;

    /// \brief The class destructor
    /// TODO: Implement the destructor
    ~ZeroTimePlanner() = default;

    bool initializePlanner(const std::shared_ptr<ims::ctmp_action_space>& actionSpacePtr,
                           const ims::BestFirstSearchParams &params,
                           const StateType& start, const StateType & goal,
                           bool query_mode = false, std::string planner_type = "RRTConnect");

    bool isQueryCovered(
            const StateType & full_start_state,
            const StateType & goal);

    void setStartAndGoal(
            const StateType & start_state,
            const StateType & goal);

    /// @brief Get a path from pre-grasp to grasp using Runge-Kutta integration (RK4).
    /// @param pre_grasp The pre-grasp state.
    /// @param robot_state The robot state, which is the initial condition (start angle) and has access to the Jacobian.
    /// @param grasp The grasp state. (TODO: Maybe call it differently?) diff vector?
    /// @param path The path from pre-grasp to grasp.
    /// @return True if a path was found, false otherwise.
    /// @TODO: Delete this function
    bool GetPathFromPreGraspToGrasp(const StateType & pre_grasp,
                                    robot_state::RobotStatePtr & robot_state,
                                    const StateType & grasp,
                                    std::vector<StateType >& path);

    /// @brief Pre-process the goal region according to the algorithm presented in the paper.
    /// @param full_start_state The start state.
    void PreProcess(const StateType & full_start_state);

    /// @brief Plan alternative paths from the start state to the goal region after adding the current path occupancy
    /// to the obstacles. TODO: Implement
    /// @param attractor The attractor state.
    /// @param current_path The current path from the start state to the attractor.
    /// @param new_paths The new paths from the start state to the attractor.
    /// @return True if a paths were found, false otherwise.
    bool PlanAlternativePaths(const StateType & attractor,
                              const std::vector<StateType >& current_path,
                              std::vector<std::vector<StateType >>& new_paths);


    /// @brief Plan a path between two states using OMPL.
    /// @param start The start state.
    /// @param goal The goal state.
    /// @param path The path from the start state to the goal state.
    /// @return True if a path was found, false otherwise.
    bool PlanPathBetweenTwoStatesOMPL(const StateType & start,
                                      const StateType & goal,
                                      std::vector<StateType >& path);

    /// @brief plan all the paths from each attractor in one goal region to all the attractors in the other goal region.
    /// @param goal_region1 The first goal region.
    /// @param goal_region2 The second goal region.
    /// @return True if a paths were found, false otherwise.
    bool PlanAllPathsBetweenGoalRegions(std::vector<region>& goal_region1,
                                        std::vector<region>& goal_region2);

    /// @brief For a specific query of end-effector pose, looks for a feasible path.
    /// @param path The path from the start state to the goal state.
    /// @param grasp_dir The directory of the grasp files.
    /// @param previous_path The previous path from the start state to previous goal state queried.
    void Query(std::vector<StateType>& path,
               std::vector<StateType> &refined_path,
               std::string grasp_dir = "",
               const std::shared_ptr<std::vector<StateType>>& previous_path = nullptr);

    /// @brief Takes the goal position (x, y, z) and iterate over all grasp options
    /// (preprocessed files). When finds a feasible solution, it returns the path.
    void GraspQuery(std::vector<StateType >& path,
                    std::string grasp_dir = "");

    /// @brief Plan path between goal1 and goal2 with experience of previous concatenated paths
    /// @param goal1 The first goal region.
    /// @param goal2 The second goal region.
    /// @param previous_path The previous concatenated path, for previous goal state to start state to current goal state
    /// @return True if a paths were found, false otherwise.
    bool PlanPathWithExperience(StateType& goal1,
                                StateType & goal2,
                                const std::shared_ptr<std::vector<StateType>>& previous_path,
                                std::vector<StateType >& path);


    /// @brief Plan using CTMP: Either preprocess or query.
    /// @param path The path from the start state to the goal state.
    bool plan(std::vector<StateType >& path,
              std::vector<StateType >& refined_path,
              const std::shared_ptr<std::vector<StateType>>& previous_path = nullptr);

    /// @brief Report stats of the planner.
    PlannerStats reportStats() const {
        return stats_;
    }

private:

    void InitMoveitOMPL();

    void InitwAstarIMS();

    /// @brief Plan a path from the start state to the attractor using OMPL.
    /// @param attractor The attractor state. IMPORTANT: The attractor state here is in configuration space!
    /// @param path The path from the start state to the attractor.
    /// @return True if a path was found, false otherwise.
    bool PlanPathFromStartToAttractorOMPL(const StateType & attractor, std::vector<StateType >& path);

    /// @brief Plan a path from the start state to the attractor using the ims search package.
    /// @param attractor The attractor state.
    /// @param path The path from the start state to the attractor.
    /// @return True if a path was found, false otherwise.
    bool PlanPathFromStartToAttractorIMS(const StateType & attractor, std::vector<StateType >& path);

    ros::NodeHandle nh;
    ros::NodeHandle m_nh;
    std::string pp_planner_;
    bool query_mode_{};

    PlannerStats stats_;

    StateType start_state_;
    StateType goal_;

//    ManipLattice* m_manip_space;
    std::shared_ptr<ims::ctmp_action_space> task_space_;

    std::shared_ptr<ims::plannerZero> planner_zero_;

    std::vector<region> regions_;
    std::vector<region> iregions_;

    std::unique_ptr<ims::wAStar> wastar_ptr_;

    std::unique_ptr<moveit::planning_interface::MoveGroupInterface> group_;
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface_;

    std::string read_write_dir_;
    std::string read_write_path_;
    std::string arm_name_;

    void WriteRegions(std::string path="");

    void ReadRegions(std::string path="");
};


#endif //CTMP_ZERO_TIME_PLANNER_HPP
