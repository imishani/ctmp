//
// Created by itamar on 7/20/23.
//

#ifndef CTMP_MANIPULATION_WS_SRC_CTMP_INCLUDE_CTMP_EXPERIENCE_PLANNER_HPP_
#define CTMP_MANIPULATION_WS_SRC_CTMP_INCLUDE_CTMP_EXPERIENCE_PLANNER_HPP_

#include <search/planners/arastar.hpp>


namespace ims {

    /// @class AStar class.
    /// @brief A* is a best first search algorithm that uses admissible heuristics and g values to find the optimal path
    class ExperienceARAstar : public ARAStar{

    public:

        /// @brief Constructor
        /// @param params The parameters
        explicit ExperienceARAstar(const ARAStarParams &params) : ARAStar(params) {}


        /// @brief Initialize the planner
        /// @param action_space_ptr The action space
        /// @param starts Vector of start states
        /// @param goals Vector of goal states
        void initializePlanner(const std::shared_ptr<ActionSpace>& action_space_ptr,
                               const std::vector<StateType>& starts,
                               const std::vector<StateType>& goals) override {
            // space pointer
            action_space_ptr_ = action_space_ptr;

            if (goals.empty() || starts.empty()) {
                throw std::runtime_error("Starts or goals are empty");
            }

            if (goals.size() > 1) {
                throw std::runtime_error("Currently, only one goal is supported");
            }

            int goal_ind_ = action_space_ptr_->getOrCreateRobotState(goals[0]);
            auto goal_ = getOrCreateSearchState(goal_ind_);
            goals_.push_back(goal_ind_);
            // Evaluate the goal state
            goal_->parent_id = PARENT_TYPE(GOAL);
            heuristic_->setGoal(const_cast<StateType &>(goals[0]));
            goal_->h = 0;

            int parent_id = PARENT_TYPE(START); double g = 0;
            for (auto &start : starts) {
                // check if start is valid
                if (!action_space_ptr_->isStateValid(start)){
                    throw std::runtime_error("Start state is not valid");
                }
                // Evaluate the start state
                int start_ind_ = action_space_ptr_->getOrCreateRobotState(start);
                auto start_ = getOrCreateSearchState(start_ind_);
                if (start_ind_ <= parent_id)
                    continue;
                start_->parent_id = parent_id;
                heuristic_->setStart(const_cast<StateType &>(start));
                start_->g = g;
                start_->h = computeHeuristic(start_ind_);
                if (parent_id == PARENT_TYPE(START))
                    params_.epsilon /= start_->h;
                start_->f = start_->g + params_.epsilon*start_->h;
                open_.push(start_);
                start_->setOpen();
                parent_id = start_ind_; g += 1000;
            }
            stats_.suboptimality = params_.epsilon;
            ROS_INFO_STREAM(CYAN << "Epsilon: " << params_.epsilon << RESET);
        }

        void updateBounds() override{
            // loop over all states in open
            double max_eps {0};
            for (SearchState *state : open_){
                double eps_suggested = (params_.curr_cost - state->g) / state->h;
                if (eps_suggested > max_eps)
                    if (eps_suggested < params_.epsilon)
                        max_eps = eps_suggested;
            }
            params_.epsilon = max_eps;
        }

    };
}


#endif //CTMP_MANIPULATION_WS_SRC_CTMP_INCLUDE_CTMP_EXPERIENCE_PLANNER_HPP_
