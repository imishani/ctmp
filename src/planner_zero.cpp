/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   actionSpace.hpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   4/12/23
*/

#include "ctmp/planner_zero.hpp"

ims::plannerZero::plannerZero(const BestFirstSearchParams &params) :
    BestFirstSearch(params),
    iteration_(1),
    call_number_(0),
    reachability_expansions_(0),
    h_max_(0){
}

auto ims::plannerZero::getSearchState(int state_id) -> ims::plannerZero::SearchState * {
    assert(state_id < states_.size() && state_id >= 0);
    return states_[state_id];
}

auto ims::plannerZero::getOrCreateSearchState(int state_id) -> ims::plannerZero::SearchState * {
    if (state_id >= states_.size()){
        states_.resize(state_id + 1, nullptr);
    }
    if (states_[state_id] == nullptr){
        assert(state_id < states_.size() && state_id >= 0);
        states_[state_id] = new SearchState;
        states_[state_id]->state_id = state_id;
    }
    return states_[state_id];
}

void ims::plannerZero::initializePlanner(const std::shared_ptr<ctmp_action_space> &actionSpacePtr, const StateType &start,
                                         const StateType &goal) {
    // space pointer
    ctmp_action_space_ = actionSpacePtr;
    action_space_ptr_ = actionSpacePtr;
    // check if start is valid
    StateType seed, joint_state_start;
    ctmp_action_space_->getCurrJointStates(seed);
    if (!ctmp_action_space_->isStateValid(start, seed, joint_state_start)){
        ROS_ERROR("Start state is not valid");
//        throw std::runtime_error("Start state is not valid");
    }
    // check if goal is valid
    StateType joint_state_goal = joint_state_start;
//    ctmp_action_space_->getJointStates(joint_state_goal);
    if (!ctmp_action_space_->isStateValid(goal, joint_state_goal)){
        ROS_ERROR("Goal state is not valid");
//        throw std::runtime_error("Goal state is not valid");
    }
    int start_ind = ctmp_action_space_->getOrCreateRobotState(start);
    auto start_robot = ctmp_action_space_->getRobotState(start_ind);
    start_robot->state_mapped = joint_state_start;
    printf("start ind: %d \n", start_ind);
    auto start_state = getOrCreateSearchState(start_ind);
    start_state->parent_id = START;
    int goal_ind = ctmp_action_space_->getOrCreateRobotState(goal);
    auto goal_state = getOrCreateSearchState(goal_ind);
    goals_.push_back(goal_ind);

//    goal_->setMappedState(joint_state_goal);
    goal_state->parent_id = GOAL;
    heuristic_->setGoal(const_cast<StateType &>(goal));
    // Evaluate the start state
    start_state->g = 0;
    start_state->h = computeHeuristic(start_ind);
    start_state->f = start_state->h;
//    open_.push(m_start);
//    m_start->setOpen();
    // Evaluate the goal state
    goal_state->h = 0;
}

void ims::plannerZero::reinitSearchState(int s_ind) {
    // check if state index has been initiated already
    if (state_initiated_.find(s_ind) == state_initiated_.end()){
        state_initiated_.insert(s_ind);
        auto s = getSearchState(s_ind);
        s->covered_this = false;
        s->greedy = false;
        s->h = INF_DOUBLE;
        s->f = INF_DOUBLE;
        s->in_open = false; s->in_closed = false;
    }
}


bool ims::plannerZero::is_state_covered(int id) {
    auto search_state = getSearchState(id);
    return search_state->covered;
}


//unsigned int ims::plannerZero::compute_reachability(unsigned int r_max, int attractor_state_id) {
double ims::plannerZero::compute_reachability(double r_max, int attractor_state_id) {
    ++call_number_;
    iteration_ = 1;
    h_max_ = 0;
    h_max_states_.clear();
    state_initiated_.clear();
    open_.clear();

    auto attractor_state = getSearchState(attractor_state_id);
    reinitSearchState(attractor_state_id);
    attractor_state->greedy = true;
    attractor_state->covered = true;
    attractor_state->h = 0;
    attractor_state->setClosed();

    preds_.clear();
    costs_.clear();
    ctmp_action_space_->getSuccessors(attractor_state_id, preds_, costs_);

    for (const auto& pred : preds_) {
        auto pred_state = getOrCreateSearchState(pred);
        reinitSearchState(pred);
        pred_state->h = computeHeuristic(pred, attractor_state_id);
        pred_state->f = pred_state->h;
        if (open_.contains(pred_state)) {
            open_.decrease(pred_state);
        } else {
            open_.push(pred_state);
        }
    }

//    unsigned int radius = 0;
    double radius = 0;

    while (radius <= r_max && !open_.empty()) {
        // getchar();
        succs_.clear();
        costs_.clear();
        auto min_state = open_.min();
        open_.pop();
        min_state->setClosed();
        reachability_expansions_++;
        if (min_state->h > h_max_) {
            h_max_states_.clear();
        }
        h_max_ = min_state->h;

        // if not previously covered
        if (!min_state->covered) {
            min_state->covered = true;
            min_state->covered_this = true;
        }

        h_max_states_.push_back(min_state);
        ////
        // Check for min_state being in joint state:
        ///@{ Greedy successor --line 7
        auto start = std::chrono::system_clock::now();
        ctmp_action_space_->getSuccessors(min_state->state_id,
                                          succs_, costs_);

        SearchState* succ_state_g = nullptr;
        double min_h = INF_DOUBLE;
        std::vector<SearchState*> greedy_succs;

        for (const auto& succ_state_id : succs_) {
            auto succ_state = getOrCreateSearchState(succ_state_id);
            reinitSearchState(succ_state_id);
//            succ_state->h = computeHeuristic(succ_state, attractor_state);
            if (succ_state->h < min_h) {
                min_h = succ_state->h;
                succ_state_g = succ_state;
            }
        }
        //tie_breaking
#ifdef false
        for (const auto& succ_id : succs_) {
            SearchState* succ_state = getSearchState(succ_id);
            if (succ_state->h == min_h) {
                greedy_succs.push_back(succ_state);
            }
        }

        for (const auto& s : greedy_succs) {
            if (task_space_->IsStateToStateValid(min_state->state_id, s->state_id)) {
                succ_state_g = s;
                break;
            }
        }
#endif
        std::chrono::duration<double, std::micro> duration = std::chrono::system_clock::now() - start;

        ///@}

        ///@{ Greedy set criteria --line 8-9
        start = std::chrono::system_clock::now();
        auto min_robot = ctmp_action_space_->getRobotState(min_state->state_id);
        StateType joint_states_min = min_robot->state_mapped;
        if (succ_state_g != nullptr && (succ_state_g->greedy &&
//            ctmp_action_space_->isStateToStateValid(min_state->getState(), succ_state_g->getState()))) {
            !joint_states_min.empty()) && ctmp_action_space_->isGraspable(joint_states_min,
                                                                          min_state->state_id,
                                                                          min_state->state_id)) {

            min_state->greedy = true;
            ctmp_action_space_->VisualizePoint(min_state->state_id, "greedy");
        }
            ///@}

            ///@{ Terminating condition --line 10-11
//        else if (ctmp_action_space_->isStateValid(min_state->getState(), joint_states_min)){
        else if (!joint_states_min.empty()){   // TODO: Its a hack. I check validity of the state when generating successors and  if valid then I save mappedstate.
            ctmp_action_space_->VisualizePoint(min_state->state_id, "exited");
            radius = min_state->h;

            // unset covered
            for (auto s : h_max_states_) {
                if (s->h == min_state->h) {
                    if (s->covered_this) {
                        s->covered = false;
                    }
                }
            }

            break;
        }
        else {
            ctmp_action_space_->VisualizePoint(min_state->state_id, "non_greedy");
        }
        duration = std::chrono::system_clock::now() - start;
        ROS_DEBUG_STREAM("Greedy set criteria: " << duration.count());
        ///@}

        ///@{ Set radius --line 12
        radius = min_state->h;

        // if (reachability_expansions_ % 100 == 0)
        //     SMPL_INFO_NAMED(SRLOG, "Radius so far: %d, max: %d", radius, r_max);
        ///@}

        ///@{ Insert Preds in Open list --line 13
        start = std::chrono::system_clock::now();
        ROS_DEBUG_NAMED("reachability", "Inserting preds in Open");
        for (const auto& pred_state_id : succs_) {   // because preds == succs
            // if (m_greedy.find(pred_id) == m_greedy.end()) {
            auto pred_state = getSearchState(pred_state_id);
            reinitSearchState(pred_state_id);
            if (!pred_state->greedy) {
                if (!pred_state->in_closed) {
                    pred_state->h = computeHeuristic(pred_state_id, attractor_state_id);
                    pred_state->f = pred_state->h;
                    if (open_.contains(pred_state)) {
                        open_.decrease(pred_state);
                    } else {
                        open_.push(pred_state);
                    }
                }
            }
        }
        duration = std::chrono::system_clock::now() - start;
        ROS_DEBUG_NAMED("REACHABILITY", "line 13: %f", duration.count());
        ///@}

        ROS_DEBUG_NAMED("REACHABILITY", "----------------------------------------");
    }

    if (open_.empty()) {
        printf("Valid Open list got empty\n");
        radius += 0.02;
    }
    /// line 14
    return radius;
}


double ims::plannerZero::search_for_valid_uncovered_states(double r_max, int iv_start_state_id) {
    ++call_number_;
    iteration_ = 1;
    h_max_ = 0;
    h_max_states_.clear();
    state_initiated_.clear();
    open_.clear();

    auto iv_start_state = getSearchState(iv_start_state_id);
    reinitSearchState(iv_start_state_id);
    iv_start_state->h = 0;
    iv_start_state->f = iv_start_state->h;
    open_.push(iv_start_state);

    double radius = 0;
    while (radius <= r_max && !open_.empty()){
        preds_.clear(); costs_.clear();
        auto min_state = open_.min();
        open_.pop();
        min_state->setClosed();

        if (min_state->h > h_max_) {
            h_max_states_.clear();
        }
        h_max_ = min_state->h;

        // if not previously covered
        if (!min_state->covered) {
            min_state->covered = true;
            min_state->covered_this = true;
        }

        h_max_states_.push_back(min_state);

        ctmp_action_space_->VisualizePoint(min_state->state_id, "invalid");
        auto min_robot = ctmp_action_space_->getRobotState(min_state->state_id);
        StateType joint_states_min = min_robot->state_mapped;
        if (ctmp_action_space_->isStateValid(min_robot->state, joint_states_min) &&
            !ctmp_action_space_->IsStateCovered(true, min_state->state_id)) {
            open_.push(min_state);
            radius = min_state->h;
            // unset covered
            for (auto s : h_max_states_) {
                if (s->h == min_state->h) {
                    if (s->covered_this) {
                        s->covered = false;
                    }
                }
            }
            break;
        }
        else {
            ctmp_action_space_->getSuccessors(min_state->state_id, preds_, costs_);
            for (auto &m_pred_id: preds_) {
                auto pred_state = getOrCreateSearchState(m_pred_id);
                reinitSearchState(m_pred_id);
                if (!pred_state->in_closed) {
                    pred_state->h = computeHeuristic(m_pred_id, iv_start_state_id);
                    pred_state->f = pred_state->h;
                    if (open_.contains(pred_state)) {
                        open_.decrease(pred_state);
                    } else {
                        open_.push(pred_state);
                    }
                }
            }
        }
        radius = min_state->h;
    }
    if (open_.empty()) {
        printf("Invalid Open list got empty\n");
        radius += 0.02;
    }
    return radius;
}


void ims::plannerZero::get_frontier_stateids(std::vector<int> &state_ids) {
    for (auto it = open_.begin(); it != open_.end(); ++it) {
        state_ids.push_back((*it)->state_id);
    }
}

bool ims::plannerZero::findGreedyPath(int goal_state_id, int attr_state_id, std::vector<int>& path){

    auto goal_state = getSearchState(goal_state_id);
    goal_state->h = computeHeuristic(goal_state_id, attr_state_id);
    int state_ind = goal_state_id;
    // get the current time
    auto start_time = std::chrono::system_clock::now();
    // max time:
    double max_time = 3; // seconds
    while (state_ind != attr_state_id ) {
        preds_.clear(); costs_.clear();
        path.push_back(state_ind);
//        auto state_robot = ctmp_action_space_->getRobotState(state_ind);
        auto state = getSearchState(state_ind);
        if (state->h < 1.79e-2) {
            break;
        }
        // check if time is more than max time
        auto current_time = std::chrono::system_clock::now();
        std::chrono::duration<double> duration = current_time - start_time;
        if (duration.count() > max_time) {
            ROS_WARN("Timeout in finding greedy path");
            ROS_INFO("Current h: %f", state->h);
            return false;
        }
        ctmp_action_space_->getSuccessors(state_ind, preds_, costs_);
        double min_cost = std::numeric_limits<double>::infinity();
        for (size_t i = 0; i < preds_.size(); ++i) {
            // get the heuristic value of the predecessor
            auto pred_state_id = preds_[(int)i];
            auto pred_state = getOrCreateSearchState(pred_state_id);
            pred_state->h = computeHeuristic(pred_state_id, attr_state_id);
            if (pred_state->h < min_cost) {
                min_cost = pred_state->h;
                state_ind = pred_state->state_id;
            }
        }
    }
    if (false) { // isTimeOut()
        ROS_WARN("Timeout in finding greedy path");
        return false;
    }
    else {
        // reverse the path
        std::reverse(path.begin(), path.end());
        return true;
    }
}

void ims::plannerZero::resetPlanningData(){
    for (auto state_ : states_){
        delete state_;
    }
    states_.clear();
    open_.clear();
    goals_.clear();
    goal_ = -1;
    stats_ = PlannerStats();
}


