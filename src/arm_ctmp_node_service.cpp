/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   arm_ctmp_node_service.cpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   5/20/23
*/

#include <ctmp/zero_time_planner.hpp>

// ROS includes
#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <actionlib/server/simple_action_server.h>
#include <std_msgs/Int8.h>

#include <ctmp/ctmpAction.h>


class ctmpActionServer
{
protected:

    ros::NodeHandle nh_;
    ros::NodeHandle pnh_;
    std::vector<std::shared_ptr<actionlib::SimpleActionServer<ctmp::ctmpAction>>> as_;  // NodeHandle instance must
                                                                                        // be created before this line.
                                                                                        // Otherwise strange error occurs.
    // create messages that are used to published feedback/result
    ctmp::ctmpActionFeedback feedback_;
    ctmp::ctmpResult result_;
    std::vector<moveit::planning_interface::MoveGroupInterfacePtr> move_groups;
    StateType discretization {};
    std::vector<std::string> robot_names;
    std::vector<std::shared_ptr<ims::MoveitInterface>> scene_interfaces;
    ims::ctmpActionType action_type;
    std::vector<std::shared_ptr<ims::ctmp_action_space>> action_spaces;

    int intention_state_ {0};
    bool move_group_status_ {false};

    ros::Subscriber intention_sub_;
    // create a feedback subscriber
    ros::Subscriber move_group_status_sub_;


public:

    ctmpActionServer(const std::vector<std::string>& robot_names,
                     ros::NodeHandle& nh,
                     ros::NodeHandle& pnh
                     ) :
            nh_(nh),
            pnh_(pnh),
            robot_names(robot_names)
    {

        // get manipulation_planning package path
        auto full_path = ros::package::getPath("manipulation_planning");
        std::string path_mprim = full_path + "/config/ws.mprim";

        action_type = ims::ctmpActionType(path_mprim);

        discretization = {0.02, 0.02, 0.02,
                          M_PI/180, M_PI/180, M_PI/180};
        action_type.Discretization(discretization);
        action_type.setSpaceType(ims::ManipulationType::SpaceType::WorkSpace); // Already the default

        intention_sub_ = nh_.subscribe("/intention_state", 1, &ctmpActionServer::intentionCallback, this);
        // go to "ready" pose first
        // get the last char as string from the robot name
        for (auto& robot_name : robot_names) {
            // register the goal and feeback callbacks
            as_.emplace_back(std::make_shared<actionlib::SimpleActionServer<ctmp::ctmpAction>>(nh_,
                    robot_name + "/ctmp_action",
                    boost::bind(&ctmpActionServer::executeCB, this, _1), false));
            move_groups.emplace_back(std::make_shared<moveit::planning_interface::MoveGroupInterface>(robot_name));
            scene_interfaces.emplace_back(std::make_shared<ims::MoveitInterface>(robot_name));
            action_spaces.emplace_back(std::make_shared<ims::ctmp_action_space>(*scene_interfaces.back(), action_type));
            move_groups.back()->setNamedTarget("ready" + robot_name.substr(robot_name.size() - 1));
            move_groups.back()->move();
            ros::Duration(0.1).sleep();
            as_.back()->start();
        }
        ros::Duration(0.5).sleep();
        move_group_status_sub_ = nh_.subscribe("/execute_trajectory/status", 1, &ctmpActionServer::MoveGroupStatusCB, this);
//        as_.start();
    }

    ~ctmpActionServer() = default;

    bool getObjPose(const std::string& obj_name,
                    std::vector<double>& obj_pose) {
        // look up the object pose
        ROS_INFO("Getting object pose");
        auto planning_interface = moveit::planning_interface::PlanningSceneInterface();
        auto collision_objects = planning_interface.getObjects();
        ROS_INFO("Number of objects: %zu", collision_objects.size());
        for (auto& collision_obj : collision_objects) {
            ROS_INFO("Object name: %s", collision_obj.second.id.c_str());
            if (collision_obj.second.id == obj_name) {
                auto object_pose = collision_obj.second.pose;
                // get the relative pose of the object from param server
                geometry_msgs::Pose obj_pose_relative;
                if (!nh_.getParam(obj_name + "/relative_pose/position/x", obj_pose_relative.position.x)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/position/y", obj_pose_relative.position.y)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/position/z", obj_pose_relative.position.z)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/orientation/x", obj_pose_relative.orientation.x)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/orientation/y", obj_pose_relative.orientation.y)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/orientation/z", obj_pose_relative.orientation.z)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                if (!nh_.getParam(obj_name + "/relative_pose/orientation/w", obj_pose_relative.orientation.w)) {
                    ROS_ERROR_STREAM("Failed to get the relative pose of the object");
                    return false;
                }
                // transform the relative pose to the absolute pose
                ims::convertFromRelativePose(object_pose, obj_pose_relative, obj_pose);
                std::cout << obj_pose[0] << obj_pose[1] << obj_pose[2] << std::endl;
                return true;
            }
        }
        return false;
    }

    void intentionCallback(const std_msgs::Int8::ConstPtr& msg) {
        if (msg->data != intention_state_)
            std::cout << "Intention state: " << (int)msg->data << std::endl;
        intention_state_ = (int)msg->data;
    }

    void MoveGroupStatusCB(const actionlib_msgs::GoalStatusArray::ConstPtr& msg) {
        if (!msg->status_list.empty() && (msg->status_list.back().status != actionlib_msgs::GoalStatus::ACTIVE))
            move_group_status_ = false;
        else
            move_group_status_ = true;
    }

    void executeCB(const ctmp::ctmpGoalConstPtr &goal)
    {
        // helper variables
        bool success = true;
        // get the robot name
        std::string robot = goal->robot_name;
        // look for the index of the robot in the robot_names vector
        auto it = std::find(robot_names.begin(), robot_names.end(), robot);
        if (it == robot_names.end()) {
            ROS_ERROR_STREAM("Robot name not found");
            success = false;
            return;
        }
        auto robot_idx = std::distance(robot_names.begin(), it);
        // get the move_group and scene_interface
        auto& as = as_[robot_idx];
        auto& move_group = move_groups[robot_idx];
        auto& scene_interface = scene_interfaces[robot_idx];
        auto& action_space = action_spaces[robot_idx];

        ros::param::set("/" + robot + "/pick", true);

        ROS_INFO("Initiated action space");
        std::vector<double> pick_pose; // x, y, z, r, p ,y
        if (!getObjPose(goal->pick_object_name, pick_pose)) {
            ROS_ERROR_STREAM("Failed to get the pose of the pick object");
            success = false;
            as->setAborted();
            return;
        }

        std::shared_ptr<PathType> previous_path = nullptr;
        // check if msg has previous trajectory
        if (!goal->previous_path.points.empty()) {
            previous_path = std::make_shared<PathType>();
            // get the previous trajectory
            for (int i {0} ; i < goal->previous_path.points.size() ; i++) {
                StateType state;
                for (int j {0} ; j < goal->previous_path.points[i].positions.size() ; j++) {
                    state.push_back(goal->previous_path.points[i].positions[j]);
                }
                previous_path->push_back(state);
            }
        }

        StateType start_state {0, 0, 0, 0, 0, 0};
        ROS_INFO("Getting current pose");
        geometry_msgs::PoseStamped current_pose = move_group->getCurrentPose();  // "arm_1tool0"

        start_state[0] = current_pose.pose.position.x;
        start_state[1] = current_pose.pose.position.y;
        start_state[2] = current_pose.pose.position.z;

        Eigen::Quaterniond current_pose_eigen;
        tf::quaternionMsgToEigen(current_pose.pose.orientation, current_pose_eigen);

        ims::get_euler_zyx(current_pose_eigen, start_state[5], start_state[4], start_state[3]);
        ims::normalize_euler_zyx(start_state[5], start_state[4], start_state[3]);

        StateType goal_state = pick_pose;

        ims::roundStateToDiscretization(goal_state, discretization);
        ims::roundStateToDiscretization(start_state, discretization);

        Eigen::Quaterniond start_pose_eigen;
        ims::from_euler_zyx(start_state[5], start_state[4], start_state[3], start_pose_eigen);
        geometry_msgs::Pose pose_check;
        pose_check.position.x = start_state[0]; pose_check.position.y = start_state[1]; pose_check.position.z = start_state[2];
        tf::quaternionEigenToMsg(start_pose_eigen, pose_check.orientation);

        // check if the inverse kinematics solution exists for the current pose and check if the solution is equal to the current joint state
        std::vector<double> current_joint_state = move_group->getCurrentJointValues();
        std::vector<double> ik_solution;

        if (!scene_interface->calculateIK(pose_check, current_joint_state, ik_solution)) {
            std::cout << "No IK solution for the current pose" << std::endl;
            // failure
//            success = false;
//            result_.success = success;
//            as->setSucceeded(result_);
//            return;
        }
        else {
            ims::rad2deg(ik_solution); ims::rad2deg(current_joint_state);
            ROS_DEBUG_STREAM("IK solution for the current pose" << std::endl);
            for (int i = 0; i < ik_solution.size(); i++) {
                std::cout << "joint " << i << " " << ik_solution[i] << " " << current_joint_state[i] << std::endl;
            }
        }
        ROS_DEBUG_STREAM( "Start state " << start_state[0] << " " << start_state[1] << " " << start_state[2] << " "
                                         << start_state[3] << " " << start_state[4] << " " << start_state[5] << std::endl);
        ROS_DEBUG_STREAM( "Goal state " << goal_state[0] << " " << goal_state[1] << " " << goal_state[2] << " "
                                         << goal_state[3] << " " << goal_state[4] << " " << goal_state[5] << std::endl);

        auto* heuristic = new ims::SE3HeuristicRPY;
        ims::BestFirstSearchParams params(heuristic);

        bool query_mode = true;
        ZeroTimePlanner planner;
        if (!planner.initializePlanner(action_space, params,
                                       start_state, goal_state,
                                       query_mode, "RRTConnect")){
            //raise error
            ROS_ERROR_NAMED("Service", "Planner initialization failed");
            // failure
            success = false;
            result_.success = success;
            as->setSucceeded(result_);
            return;
        }

        std::vector<StateType> path_;
        std::vector<StateType> not_refined_path;
        if (!planner.plan(not_refined_path, path_, previous_path)) {
            ROS_ERROR("No path found");
            // failure
            success = false;
            result_.success = success;
            as->setSucceeded(result_);
            return;
        }

        std::vector<StateType> path;
        // nake sure the current state is the first state
        StateType path_start_state;
        move_group->getCurrentState()->copyJointGroupPositions(move_group->getName(), path_start_state);
        path.push_back(path_start_state);
        path.insert(path.end(), path_.begin(), path_.end());

        moveit_msgs::RobotTrajectory trajectory;
        double max_velocity_scaling_factor {0.4};
        double max_acceleration_scaling_factor {0.4};

        try {
            std::cout << YELLOW << "Intention state: " << intention_state_ << RESET << std::endl;
            if (intention_state_ == 1) {
                max_velocity_scaling_factor /= 4;
                max_acceleration_scaling_factor /= 4;
            }
        }
        catch (ros::Exception& e) {
            ROS_ERROR_STREAM("Failed to read /intention_state topic");
        }

        // go down and grasp the object
        // get the current pose of the end effector
        Eigen::VectorXd pre_grasp_vec(path.back().size());
        // get the last state form path
        pre_grasp_vec[0] = path.back()[0]; pre_grasp_vec[1] = path.back()[1]; pre_grasp_vec[2] = path.back()[2];
        pre_grasp_vec[3] = path.back()[3]; pre_grasp_vec[4] = path.back()[4]; pre_grasp_vec[5] = path.back()[5];
        Eigen::VectorXd grasp_vec = pre_grasp_vec;
        if (robot == "manipulator_1") {
            grasp_vec[2] -= 0.16;
        }
        else if (robot == "manipulator_2") {
            // move the arm based on path
            grasp_vec[2] -= 0.16;
        }

        // get the jacobian
        Eigen::MatrixXd J;
        StateType theta = path.back();
        auto robot_state = move_group->getCurrentState();
        robot_state->setJointGroupPositions(robot, theta);
        auto joint_model_group = robot_state->getJointModelGroup(robot);
        robot_state->getJacobian(joint_model_group, robot_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                                 Eigen::Vector3d(0, 0, 0), J);
        double h = 1 / 10.0;

        ROS_INFO_STREAM("theta " << theta[0] << " " << theta[1] << " " << theta[2] << " " << theta[3] << " " << theta[4] << " " << theta[5]);
        Eigen::MatrixXd J_inv;
        PathType grasp_path;
        grasp_path.push_back(theta);
        for (int i{0} ; i < 10; i++){
            J_inv = J.transpose() * (J * J.transpose()).inverse();
            // RK(4)
            auto k1 = h * (J_inv * (grasp_vec - pre_grasp_vec));
            auto k2 = h * (J_inv * (grasp_vec - pre_grasp_vec + k1 / 2.0));
            auto k3 = h * (J_inv * (grasp_vec - pre_grasp_vec + k2 / 2.0));
            auto k4 = h * (J_inv * (grasp_vec - pre_grasp_vec + k3));
            auto del_theta = (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
            for (size_t j = 0; j < 6; ++j) {
                theta[j] += del_theta[j];
            }
            robot_state->setJointGroupPositions(robot, theta);
            robot_state->update();
            // check if the new state is in collision and out of bounds
            if (!robot_state->satisfiesBounds(joint_model_group)) {
                ROS_WARN("Out of bounds");
            }
            // check if the new state is in collision
            collision_detection::CollisionRequest collision_request;
            collision_detection::CollisionResult collision_result;
            collision_request.group_name = robot;
            collision_result.clear();
            scene_interface->planning_scene_->checkCollision(collision_request, collision_result, *robot_state);
            if (collision_result.collision) {
                ROS_WARN("In collision");
//                break;
            }
            grasp_path.push_back(theta); path.push_back(theta);
            robot_state->getJacobian(joint_model_group, robot_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                                     Eigen::Vector3d(0, 0, 0), J);
        }

//        ims::ShortcutSmooth(path, *move_group,
//                            scene_interface->mPlanningScene);

        ims::profileTrajectory(path.at(0),
                               path.back(),
                               path,
                               *move_group,
                               trajectory,
                               max_velocity_scaling_factor,
                               max_acceleration_scaling_factor);

        // make sure it will publish the status to the move_group/status topic
        move_group->execute(trajectory);
        // get the time of the trajectory execution
        double trajectory_time = trajectory.joint_trajectory.points.back().time_from_start.toSec();

        geometry_msgs::Pose place_pose = goal->place_pose;
        goal_state[0] = place_pose.position.x;
        goal_state[1] = place_pose.position.y;
        goal_state[2] = place_pose.position.z;

        Eigen::Quaterniond place_pose_eigen;
        tf::quaternionMsgToEigen(place_pose.orientation, place_pose_eigen);
        ims::get_euler_zyx(place_pose_eigen, goal_state[5], goal_state[4], goal_state[3]);
        ims::normalize_euler_zyx(goal_state[5], goal_state[4], goal_state[3]);
        ROS_INFO("goal state %f %f %f %f %f %f", goal_state[0], goal_state[1], goal_state[2], goal_state[3], goal_state[4], goal_state[5]);
        // discretize the goal state
        for (int i = 0; i < 6; i++) {
            goal_state[i] = std::round(goal_state[i] / discretization[i]) * discretization[i];
        }
        ROS_INFO("goal state %f %f %f %f %f %f", goal_state[0], goal_state[1], goal_state[2], goal_state[3], goal_state[4], goal_state[5]);

        // check IK
        if (scene_interface->calculateIK(place_pose, ik_solution)) {
            ROS_DEBUG("IK solution for the place pose");
            for (int i = 0; i < ik_solution.size(); i++) {
                ROS_DEBUG_STREAM("joint " << i << " " << ik_solution[i]*180/M_PI << " " << current_joint_state[i] << std::endl);
            }
        }
        else {
            ROS_ERROR("No IK solution for the place pose");
            // failure
//            success = false;
//            as->setAborted();
        }

        ros::param::set("/" + robot + "/pick", false);
        ros::param::set("/refinement_time_limit", trajectory_time);
        action_space->readGoalRegions();

        ZeroTimePlanner planner2;
        if (!planner2.initializePlanner(action_space, params,
                                        start_state, goal_state,
                                        query_mode, "RRTConnect")){
            //raise error
            ROS_ERROR("Planner initialization failed");
            // failure
            result_.success = false;
            as->setSucceeded(result_);
            return;
        }

        // attach the object
        move_group->attachObject(goal->pick_object_name);
        ros::Duration(0.1).sleep();

        std::vector<StateType> path_2, not_refined_path_2;
        std::shared_ptr<std::vector<StateType>> path_ptr = nullptr;
        if (previous_path == nullptr){
            path_ptr = std::make_shared<std::vector<StateType>>(path);
        } else {
            path_ptr = std::make_shared<std::vector<StateType>>(not_refined_path);
        }
        if (!planner2.plan(not_refined_path_2, path_2, path_ptr)) {
            ROS_ERROR("No path found");
            // failure
            success = false;
            // just send failure, don't crash
            result_.success = false;
            as->setSucceeded(result_);
            return;
        }
        move_group->getCurrentState()->copyJointGroupPositions(move_group->getName(), path_start_state);
        path_2.insert(path_2.begin(), path_start_state);
        not_refined_path_2.insert(not_refined_path_2.begin(), path_start_state);
        ROS_INFO("Path to pre-place pose found!");
        trajectory.joint_trajectory.points.clear();
//        ims::ShortcutSmooth(path_2, *move_group,
//                            scene_interface->mPlanningScene);
        ims::profileTrajectory(path_2.at(0),
                               path_2.back(),
                               path_2,
                               *move_group,
                               trajectory);

        // check if previous execution is finished
        while (move_group_status_) {
            ros::Duration(0.1).sleep();
        }

        if (robot == "manipulator_2") {
            // close the gripper using gripper_2 group
            moveit::planning_interface::MoveGroupInterface gripper_2("gripper_2");
            gripper_2.setJointValueTarget("arm_2robotiq_85_left_knuckle_joint", 0.6);
            // increase the speed
            gripper_2.setMaxVelocityScalingFactor(0.8);
            gripper_2.move();
            ros::Duration(0.1).sleep();
        }

        move_group->execute(trajectory);
        // move down, release and go up
        // get current pose
//        ros::Duration(0.1).sleep();
//        current_pose = move_group->getCurrentPose();
//        current_pose.pose.position.z -= 0.5;
//        move_group->setPoseTarget(current_pose);
//        move_group->move();
//        ros::Duration(0.1).sleep();

        if (robot == "manipulator_2") {
            // define the move group
            moveit::planning_interface::MoveGroupInterface gripper_2("gripper_2");
            gripper_2.setNamedTarget("open_2");
            gripper_2.setMaxVelocityScalingFactor(0.8);
            gripper_2.move();
            ros::Duration(0.1).sleep();
        }
        // detach the object
        move_group->detachObject(goal->pick_object_name);
        ros::Duration(0.1).sleep();
//        current_pose = move_group->getCurrentPose();
//        current_pose.pose.position.z += 0.5;
//        move_group->setPoseTarget(current_pose);
//        move_group->move();

        // cache the not_refined_path
        trajectory = moveit_msgs::RobotTrajectory();
        ims::profileTrajectory(not_refined_path_2.at(0),
                               not_refined_path_2.back(),
                               not_refined_path_2,
                               *move_group,
                               trajectory);
        // cache trajectory in data directory
        std::ofstream file;
        std::string file_path = ros::package::getPath("ctmp") + "/data/trajectory.txt";
        file.open(file_path);
        for (int i {0} ; i < trajectory.joint_trajectory.points.size() ; i++) {
            for (int j {0} ; j < trajectory.joint_trajectory.points[i].positions.size() ; j++) {
                file << trajectory.joint_trajectory.points[i].positions[j] << " ";
            }
            file << std::endl;
        }
        file.close();

        if(success)
        {
            result_.success = true;
            ROS_INFO("%s: Succeeded", robot.c_str());
            // set the action state to succeeded
            as->setSucceeded(result_);
            return;
        }
    }
};



int main(int argc, char** argv) {

    std::vector<std::string> robots;
    // loop through the arguments and if there is an argument that starts with a "manipulator" then it is a robot name
    for (int i = 0; i < argc; i++) {
        std::string arg = argv[i];
        if (arg.find("manipulator") != std::string::npos) {
            robots.push_back(arg);
        }
    }
    // initialize ROS
    ros::init(argc, argv, "~");
    ros::NodeHandle nh;
    ros::AsyncSpinner spinner(8);
    spinner.start();

    // Before I will load it with launch file, lets set params to ROS server of the limits of the goal region:
    ros::param::set("/manipulator_1/regions/pick_region/min_limits",
                    std::vector<double>{-0.4, 0.55, 1.15, 0, 0, 0.0});
    ros::param::set("/manipulator_1/regions/pick_region/max_limits",
                    std::vector<double>{0.4, 1.0, 1.30, 0, 0, 0.0});
    ros::param::set("/manipulator_1/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 1.1, 0, 0, -0.1});
    ros::param::set("/manipulator_1/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.1, 0, 0, 0.1});

    ros::param::set("/ctmp_test/pick", true);
    ros::param::set("/manipulator_2/regions/pick_region/min_limits",
                    std::vector<double>{1.1, 0.5, 1.1 , 0.0, 0.0, 0.0});
    ros::param::set("/manipulator_2/regions/pick_region/max_limits",
                    std::vector<double>{1.4, -0.8, 1.25, 0.0, 0.0, 0.0});
    ros::param::set("/manipulator_2/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 1.1, 0.0, 0.0, 1.470796});
    ros::param::set("/manipulator_2/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.1, 0.0, 0.0, 1.570796});

    ros::param::set("/manipulator_3/regions/pick_region/min_limits",
                    std::vector<double>{-0.4, -1.0, 1.15, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/pick_region/max_limits",
                    std::vector<double>{0.4, -0.55, 1.30, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 1.05, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.4, 0, 0, 0.0});


    // create the action server
    ros::NodeHandle pnh("~");
    ctmpActionServer ctmp_action_server(robots, nh, pnh);


    ros::waitForShutdown();
//    ros::spin();
    return 0;
}