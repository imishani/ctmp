/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   zero_time_planner.hpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   4/12/23
*/

#include <memory>

#include "ctmp/zero_time_planner.hpp"
#include "ctmp/experience_planner.hpp"


bool ZeroTimePlanner::initializePlanner(const std::shared_ptr<ims::ctmp_action_space> &actionSpacePtr,
                                        const ims::BestFirstSearchParams &params, const StateType &start,
                                        const StateType &goal, bool query_mode,
                                        std::string planner_type) {
    task_space_ = actionSpacePtr;
    arm_name_ = task_space_->getPlanningGroupName();
    start_state_ = start;
    goal_ = goal;
    query_mode_ = query_mode;
    pp_planner_ = planner_type;
    // initialize planner_zero_
    planner_zero_ = std::make_shared<ims::plannerZero>(params);
    try {
        planner_zero_->initializePlanner(actionSpacePtr,
                                         start, goal);
    }
    catch (std::exception &e) {
        ROS_ERROR("Failed to initialize planner zero");
        return false;
    }
    std::string current_dir = ros::package::getPath("ctmp");
    read_write_dir_ = current_dir + "/data/";
    bool m_pick;
    nh = ros::NodeHandle();
    m_nh = ros::NodeHandle(arm_name_);
    m_nh.param("pick", m_pick, false);
    if (m_pick){
        // get current directory
        read_write_path_ = read_write_dir_ + arm_name_ + "_pick.dat";
    }
    else {
        read_write_path_ = read_write_dir_ + arm_name_ + "_place.dat";
    }
    ROS_INFO("Read write path: %s", read_write_path_.c_str());
    if (regions_.empty()) {
        ReadRegions();
        task_space_->PassRegions(&regions_, &iregions_);
    }
    return true;
}


bool ZeroTimePlanner::isQueryCovered(const StateType &full_start_state, const StateType &goal) {
    // check if preprocessing covered this query
    if (!task_space_->IsQueryCovered(full_start_state, goal)) {
        return false;
    }

    return true;
}

void ZeroTimePlanner::setStartAndGoal(const StateType &start_state, const StateType &goal) {
    start_state_ = start_state;
    goal_ = goal;
}

void ZeroTimePlanner::PreProcess(const StateType &full_start_state) {
    regions_.clear();
    ROS_INFO("Preprocessing");
    if (pp_planner_ == "RRTConnect")
        InitMoveitOMPL();
    else
        InitwAstarIMS();

//        unsigned int radius_max_v = 1;
    double radius_max_v = 0.2;
//        unsigned int radius_max_i = 100;
    double radius_max_i = 0.2;
    ROS_INFO("Waiting for regions");
    task_space_->PassRegions(&regions_, &iregions_);
    ROS_INFO("Preprocessing with %s", pp_planner_.c_str());
    // 1. SAMPLE ATTRACTOR
    int maximum_tries = 10000;
    StateType sampled_state;
    // also sets the attractor as goal for heuristic functions
    int sampled_state_id = task_space_->SampleAttractorState(sampled_state, maximum_tries);
    planner_zero_->getOrCreateSearchState(sampled_state_id);
    if (sampled_state_id == -1) {
        ROS_ERROR("Failed to sample first attractor");
        return;
    }
    task_space_->VisualizePoint(sampled_state_id, "attractor");

    while (!task_space_->m_valid_front.empty() || !task_space_->m_invalid_front.empty()) {
        while (!task_space_->m_valid_front.empty()) {

            int attractor_state_id = task_space_->SetAttractorState();

            if (!planner_zero_->is_state_covered(attractor_state_id)) { // TODO: Make sure we add the state to planner_zeto!
                task_space_->VisualizePoint(attractor_state_id, "attractor");
                std::vector<StateType> path;
                // 2. PLAN PATH TO ACTUAL GOAL

                bool ret;
                auto attr_state = task_space_->getRobotState(attractor_state_id);
                if (attr_state->state_mapped.empty()){
                    StateType mapped_state;
                    task_space_->getIKSolution(attr_state->state, mapped_state);
                    attr_state->state_mapped = mapped_state;
                }
                if (pp_planner_ == "RRTConnect") {
                    ret = PlanPathFromStartToAttractorOMPL(attr_state->state_mapped, path);
                } else {
                    ret = PlanPathFromStartToAttractorIMS(attr_state->state, path);
                }
                if (!ret){
                    continue;
                }

                // plan alternatives
//                std::vector<std::vector<StateType > alternatives;
//                bool succ_alternatives = PlanAlternativePaths(attr_state->getMappedState(), path, alternatives);
//                robot_state::RobotStatePtr checking_state = group_->getCurrentState();
//                std::vector<StateType grasp_path;
//                checking_state->setJointGroupPositions(group_->getName(), attr_state->getMappedState());
//                GetPathFromPreGraspToGrasp(attr_state->getState(),
//                                           checking_state, attr_state->getState(),
//                                           grasp_path);

                // getchar();
                // 3. COMPUTE REACHABILITY
                task_space_->UpdateSearchMode(REACHABILITY);
                // reachability search
//                    unsigned int radius = planner_zero_->compute_reachability(radius_max_v, attractor_state_id);
                double radius = planner_zero_->compute_reachability(radius_max_v, attractor_state_id);

                // 4. ADD REGION
                region r;
                r.start = full_start_state;
                r.radius = radius;
                r.state = attr_state->state;
                r.path = path;
                regions_.push_back(r);

                ROS_INFO("Radius %f, Regions so far %zu", radius, regions_.size());
                ROS_INFO("Path size: %zu", r.path.size());
                // ROS_INFO regions_:

                std::vector<int> open;
                planner_zero_->get_frontier_stateids(open);

                task_space_->FillFrontierLists(open);

            }
        }
        while (!task_space_->m_invalid_front.empty()) {

            int iv_start_state_id = task_space_->SetInvalidStartState();

            if (!planner_zero_->is_state_covered(iv_start_state_id)) {
                task_space_->VisualizePoint(sampled_state_id, "attractor");

//                    int radius = planner_zero_->search_for_valid_uncovered_states(radius_max_i, iv_start_state_id);
                double radius = planner_zero_->search_for_valid_uncovered_states(radius_max_i, iv_start_state_id);
                region r;
                r.radius = radius;
                r.state = task_space_->getRobotState(iv_start_state_id)->state;
                iregions_.push_back(r);

                ROS_INFO("Radius %f, IRegions so far %zu", radius, iregions_.size());

                std::vector<int> open;
                planner_zero_->get_frontier_stateids(open);
                task_space_->FillFrontierLists(open);

                if (!task_space_->m_valid_front.empty()) {
                    break;
                }
            }
        }
    }
    task_space_->PruneRegions();
    WriteRegions();
}

bool ZeroTimePlanner::PlanAlternativePaths(const StateType & attractor,
                                           const std::vector<StateType>& current_path,
                                           std::vector<std::vector<StateType>>& new_paths) {
    auto df = ims::getEmptyDistanceField();
    ros::Publisher one_marker_pub = nh.advertise<visualization_msgs::Marker>("occ_marker", 0);
    ros::Duration(0.1).sleep();
    int num_paths {1};
    for (auto& state : current_path){
        moveit::core::RobotState robot_state = *group_->getCurrentState();
        // print the robot state joints
        robot_state.setJointGroupPositions(group_->getName(), state);
        std::vector<std::vector<int>> occupancy_grid;
        ims::getRobotOccupancy(df, robot_state, group_, occupancy_grid);

    }
    unsigned int count_occ_cells = ims::countOccupiedCells(*df);
    // visualize all occupied cells
    ims::visualizeOccupancy(df, one_marker_pub, group_->getPlanningFrame(), num_paths++);
    ros::Duration(0.1).sleep();
    return true;

}


void ZeroTimePlanner::Query(std::vector<StateType> &path,
                            std::vector<StateType> &refined_path,
                            std::string grasp_dir,
                            const std::shared_ptr<std::vector<StateType>>& previous_path) {
    auto start_time = std::chrono::system_clock::now();
    task_space_->UpdateSearchMode(QUERY);
    if (grasp_dir.empty()){
        grasp_dir = read_write_dir_;
    }
    if (previous_path == nullptr){
        GraspQuery(path, grasp_dir);
        auto find_time = std::chrono::system_clock::now();
        std::chrono::duration<double> find_time_elapsed = find_time - start_time;
        stats_.time = find_time_elapsed.count() * 1000.0;
        bool improve;
        m_nh.param("improve", improve, true);
        if (!path.empty() && improve){
            StateType start_state = path.front();
            StateType goal_state = path.back();
            std::shared_ptr<std::vector<StateType>> path_ptr = std::make_shared<std::vector<StateType>>(path);
            bool improved_path = PlanPathWithExperience(start_state, goal_state, path_ptr, refined_path);
            if (!improved_path){
                ROS_INFO("No improved path found");
            }
            else {
                ROS_INFO("Improved path found");
            }
        }
        return;
    } else {
        std::vector<StateType> current_queried_path;
        GraspQuery(current_queried_path, grasp_dir);
        if (current_queried_path.empty()){
            return;
        } else {
            path = current_queried_path;
            // reverse the previous path and append the current path
            std::vector<StateType> previous_queried_path = *previous_path;
            std::reverse(previous_queried_path.begin(), previous_queried_path.end());
            std::vector<StateType> concat_path;
            concat_path.insert(concat_path.end(), previous_queried_path.begin(), previous_queried_path.end());
            concat_path.insert(concat_path.end(), current_queried_path.begin(), current_queried_path.end());
            std::shared_ptr<std::vector<StateType>> concat_path_ptr = std::make_shared<std::vector<StateType>>(concat_path);

            StateType start_state = concat_path.front();
            StateType goal_state = concat_path.back();

            bool improved_path = PlanPathWithExperience(start_state, goal_state, concat_path_ptr, refined_path);
            if (!improved_path){
                ROS_INFO("No improved path found");
            }
            else {
                ROS_INFO("Improved path found");
            }
            auto find_time = std::chrono::system_clock::now();
            std::chrono::duration<double> find_time_elapsed = find_time - start_time;
            stats_.time = find_time_elapsed.count() * 1000.0;
        }
    }
}

void ZeroTimePlanner::GraspQuery(std::vector<StateType> &path, std::string grasp_dir) {
    ROS_INFO("Grasp dir: %s", grasp_dir.c_str());
    boost::filesystem::path dir(grasp_dir);
    // Loop over all files in directory
    boost::filesystem::directory_iterator end_itr;
    for (boost::filesystem::directory_iterator itr(dir); itr != end_itr; ++itr) {
        std::string grasp_dir_ = itr->path().string();
        std::string grasp_name = itr->path().filename().string();
        // check if group name is a part of the file name. If not, continue
        if (grasp_name.find(task_space_->getPlanningGroupName()) == std::string::npos) {
            continue;
        }
        ROS_INFO("Grasp dir: %s", grasp_dir_.c_str());
        ROS_INFO("Grasp: %s", grasp_name.c_str());

        regions_.clear(); iregions_.clear();
        task_space_->PassRegions(&regions_, &iregions_);

        ReadRegions(grasp_dir_);
        ROS_INFO("Regions: %zu", regions_.size());

        // TODO: Add a check to see if goal is in global XYZ goal region
        // get current time

        ROS_INFO("Looking for region containing start state");
        // TODO: Make sure about the false here. Do I want to look for the closest region with or without heuristic?
        int reg_idx = task_space_->FindRegionContainingState_WS(goal_, false);

        if (reg_idx == -1){
            ROS_INFO_STREAM("Query state not covered in file: " << grasp_name);
            continue;
        }

        ROS_INFO("Region index: %d", reg_idx);
        auto region = regions_[reg_idx];
        // get or create both states
        auto attr_state_ind = task_space_->getOrCreateRobotState(region.state);
        auto attr_state = task_space_->getRobotState(attr_state_ind);
        if (attr_state->state_mapped.empty()){
            // set the mapped state to be the last state in the path
            attr_state->state_mapped = region.path.back();
        }
        auto goal_state_ind = task_space_->getOrCreateRobotState(goal_);
        auto goal_state = task_space_->getRobotState(goal_state_ind);
        // TODO: make sure the following is ok
//        if (goal_state->getMappedState().empty()){
//            goal_state->setMappedState(attr_state->getMappedState());
//        }

        std::vector<int> greedy_path;
        if (!planner_zero_->findGreedyPath(goal_state_ind, attr_state_ind, greedy_path)){
            ROS_INFO("No greedy path found");
            continue;
        }
        ROS_INFO("Greedy path size: %zu", greedy_path.size());
        path = region.path;
        // loop over the greedy path and get the IK of all states. Add to greedy path to the path
        auto seed = attr_state->state_mapped;
        for (auto state_ind : greedy_path){
            auto state_ = task_space_->getRobotState(state_ind);
//            if (state_->getMappedState().empty()){
            StateType joint_state;
            task_space_->getIKSolution(state_->state, seed, joint_state);
//            task_space_->getIKSolution(state_->state, joint_state);
            state_->state_mapped = joint_state;
//            }
            path.push_back(state_->state_mapped);   // state_->getState()
            seed = state_->state_mapped;
        }
        break;
    }
}

bool ZeroTimePlanner::PlanPathWithExperience(StateType &goal1,
                                             StateType &goal2,
                                             const std::shared_ptr<std::vector<StateType>> &subopt_path,
                                             std::vector<StateType >& path) {
    // get the motion primitives path
    std::string path_mprim = ros::package::getPath("manipulation_planning") + "/config/manip_6dof.mprim";
//    auto* heuristic = new ims::JointAnglesHeuristic;
    auto df = ims::getDistanceFieldMoveIt();
    auto* heuristic = new ims::BFSHeuristic(df, "manipulator_1"); //_3
    heuristic->setInflationRadius(0.1);
    ims::ARAStarParams params(heuristic, 1000.0*(double)(subopt_path->size() - 1), 2);

    double refinement_time_limit;
    m_nh.param("refinement_time_limit", refinement_time_limit, 0.5);
    params.ara_time_limit = refinement_time_limit - stats_.time/1000.0;
    std::shared_ptr<ims::MoveitInterface> scene_interface = task_space_->getMoveitInterface();
    ims::ManipulationType action_type(path_mprim);
    StateType discretization {1, 1, 1, 1, 1, 1};
    ims::deg2rad(discretization);
    action_type.Discretization(discretization);

    std::shared_ptr<ims::ManipulationActionSpace> action_space = std::make_shared<ims::ManipulationActionSpace>(*scene_interface, action_type, heuristic);

    ims::roundStateToDiscretization(goal1, discretization); ims::roundStateToDiscretization(goal2, discretization);
    auto goal1_from_path = subopt_path->at(0); auto goal2_from_path = subopt_path->back();
    ims::roundStateToDiscretization(goal1_from_path, discretization); ims::roundStateToDiscretization(goal2_from_path, discretization);

    std::vector<std::pair<double, double>> joint_limits;
    scene_interface->getJointLimits(joint_limits);
    ims::normalizeAngles(goal1, joint_limits); ims::normalizeAngles(goal2, joint_limits);
    ims::normalizeAngles(goal1_from_path, joint_limits); ims::normalizeAngles(goal2_from_path, joint_limits);
    // check if the goal states are the same as the ones from the path
    assert(goal1 == goal1_from_path); assert(goal2 == goal2_from_path);

    std::vector<StateType> starts {goal1};
    // loop through the subopt_path without the first and last states and add them to the starts
    for (size_t i = 1; i < subopt_path->size(); ++i){
        auto state = subopt_path->at(i);
        ims::roundStateToDiscretization(state, discretization);
        ims::normalizeAngles(state, joint_limits);
        starts.push_back(state);
    }

    ims::ExperienceARAstar planner(params);
    try {
        planner.initializePlanner(action_space, starts, std::vector<StateType> {goal2});
    }
    catch (std::exception& e){
        ROS_ERROR_STREAM("Exception: " << e.what());
        path = *subopt_path;
        return false;
    }
    if (!planner.plan(path)){
        ROS_INFO("Could not find path with experience");
        path = *subopt_path;
        return false;
    }
    else{
        PlannerStats stats = planner.reportStats();
        stats_.time += 1000*stats.time;
        ROS_INFO_STREAM(std::endl << GREEN << "Planning time: " << stats.time << " sec" << std::endl <<
        "Cost: " << stats.cost << std::endl <<
        "Path length: " << path.size() << std::endl<< "Number of nodes expanded: " << stats.num_expanded << std::endl <<
        "Suboptimality: " << stats.suboptimality << RESET << std::endl);
        return true;
    }
}

void ZeroTimePlanner::InitMoveitOMPL() {
    group_ = std::make_unique<moveit::planning_interface::MoveGroupInterface>(arm_name_);
    ROS_INFO("Planning path with OMPL");
    group_->setPlanningTime(10.0);
    group_->setPlannerId("BiTRRT");
}

void ZeroTimePlanner::InitwAstarIMS() {
    group_ = std::make_unique<moveit::planning_interface::MoveGroupInterface>(arm_name_);
    ROS_INFO("Planning path with IMS");
    double weight = 10.0;
    auto* heu = new ims::SE3HeuristicRPY;
    ims::wAStarParams wAparams (heu, weight);
    wastar_ptr_ = std::make_unique<ims::wAStar>(wAparams);
}

bool ZeroTimePlanner::PlanPathFromStartToAttractorOMPL(const StateType &attractor, std::vector<StateType> &path) {

    ROS_INFO("Planning path with OMPL");

    // start -> actual start
    // goal -> attractor

    ros::AsyncSpinner spinner(1);
    spinner.start();

    group_->setStartStateToCurrentState();

    robot_state::RobotState goal_state(*group_->getCurrentState());
    goal_state.setJointGroupPositions(arm_name_, attractor);    // "manipulator_1"
    group_->setJointValueTarget(goal_state);

    ROS_INFO_STREAM(group_->getName());
    // plan
    ROS_INFO("Going to plan!");
    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    auto ret = group_->plan(my_plan);
    sleep(1); // Wanted to 0.1 but 'sleep' takes unsigned ints only. TODO: Check other sleep method

    if (ret != moveit_msgs::MoveItErrorCodes::SUCCESS) {
        ROS_WARN("OMPL failed to plan");
        return false;
    }
    else {
        ROS_INFO("Solution found by OMPL");
    }

    // fill path
    path.resize(my_plan.trajectory_.joint_trajectory.points.size());
    for (size_t i = 0; i < my_plan.trajectory_.joint_trajectory.points.size(); ++i) {
        auto positions = my_plan.trajectory_.joint_trajectory.points[i].positions;
        path[i] = positions;
    }
    spinner.stop();

    return true;
}

bool ZeroTimePlanner::PlanPathFromStartToAttractorIMS(const StateType &attractor, std::vector<StateType> &path) {
    StateType start_state;
    task_space_->getCurrWorkspaceState(start_state);
    ROS_INFO_STREAM("Start state: " << start_state[0] << " " << start_state[1] << " " << start_state[2]
                    << " " << start_state[3] << " " << start_state[4] << " " << start_state[5] << std::endl);
    ROS_INFO_STREAM("Attractor: " << attractor[0] << " " << attractor[1] << " " << attractor[2]
                    << " " << attractor[3] << " " << attractor[4] << " " << attractor[5] << std::endl);
    // TODO: Lets assume for the moment that the attractor is discretized
    try{
        wastar_ptr_->initializePlanner(task_space_, start_state, attractor);
    }
    catch (std::exception& e) {
        std::cout << e.what() << std::endl;
        return false;
    }
    std::vector<StateType> ims_path;
    if (!wastar_ptr_->plan(ims_path)){
        ROS_WARN("IMS failed to plan");
        return false;
    }
    else {
        ROS_INFO("Solution found by IMS");
    }
    path.resize(ims_path.size());
    for (size_t i = 0; i < ims_path.size(); ++i) {
        auto state = ims_path[i];
        path[i] = state;
    }
    return true;
}

void ZeroTimePlanner::WriteRegions(std::string path) {
    // sort
    std::sort(regions_.begin(), regions_.end(), [] (const region &a,
                                                    const region &b)
    {
        return (a.radius > b.radius);
    });
    if (path.empty()){
        path = read_write_path_;
    }
    ROS_INFO("Writing regions to file");
    boost::filesystem::path myFile = path; //boost::filesystem::current_path() /
    std::cout << myFile;
    boost::filesystem::ofstream ofs(myFile);
    boost::archive::text_oarchive ta(ofs);
    ta << regions_;
}


void ZeroTimePlanner::ReadRegions(std::string path) {

    ROS_INFO("Reading regions from file");
    // getchar();
    try {
        if (path.empty()){
            path = read_write_path_;
        }
        boost::filesystem::path myFile = path; // boost::filesystem::current_path() /
        boost::filesystem::ifstream ifs(myFile/*.native()*/);
        boost::archive::text_iarchive ta(ifs);
        ta >> regions_;
    }
    catch (...) {
        ROS_WARN("Unable to read preprocessed file");
    }
}

bool ZeroTimePlanner::plan(std::vector<StateType> &path,
                           std::vector<StateType> &refined_path,
                           const std::shared_ptr<std::vector<StateType>>& previous_path) {
    if (!query_mode_){
        ROS_INFO("Preprocessing goal regions");
        int start_ind = task_space_->getOrCreateRobotState(start_state_);
        PreProcess(task_space_->getRobotState(start_ind)->state_mapped);
        return false;
    }
    else {
        ROS_INFO("Querying");
        Query(path, refined_path, "", previous_path); // TODO: Fix this, you should deal with grasp_dir better
        if (path.empty()) {
            ROS_WARN("No path found");
            return false;
        } else {
            ROS_INFO("Path found");
            return true;
        }
    }
}

bool ZeroTimePlanner::GetPathFromPreGraspToGrasp(const StateType &pre_grasp, robot_state::RobotStatePtr &robot_state,
                                                 const StateType &grasp, std::vector<StateType> &path) {
    // Assuming that the pre_grasp and grasp are (x, y, z, r, p, y)
    // initial state (configuration space)
    std::vector<double> theta0;
    Eigen::VectorXd grasp_vec(6);
    Eigen::VectorXd pre_grasp_vec(6);
    for (size_t i = 0; i < 6; ++i) {
        grasp_vec[(long)i] = grasp[i];
        pre_grasp_vec[(long)i] = pre_grasp[i];
    }
    pre_grasp_vec[2] += 0.2;

    robot_state->copyJointGroupPositions(arm_name_, theta0);
    path.push_back(theta0);
    // get the Jacobian
    Eigen::MatrixXd J;
    auto joint_model_group = robot_state->getJointModelGroup(arm_name_);
    robot_state->getJacobian(joint_model_group, robot_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                             Eigen::Vector3d(0, 0, 0), J);
    // get the Jacobian pseudo inverse

//    Eigen::MatrixXd J_pinv;
//    Eigen::JacobiSVD<Eigen::MatrixXd> svd(J, Eigen::ComputeThinU | Eigen::ComputeThinV);
//    double tolerance = std::max(J.cols(), J.rows()) * svd.singularValues().array().abs().maxCoeff() * std::numeric_limits<double>::epsilon();
//    J_pinv = svd.matrixV() * Eigen::MatrixXd(svd.singularValues().array().abs().inverse().matrix().asDiagonal()) * svd.matrixU().transpose();

    double h = 1 / 10.0;
    auto theta = theta0;
    auto curr_grasp_vec = pre_grasp_vec;
    Eigen::MatrixXd J_inv;
    for (int i{0} ; i < 10; i++){
        J_inv = J.transpose() * (J * J.transpose()).inverse();
        // RK(4)
        auto k1 = h * (J_inv * (grasp_vec - pre_grasp_vec));
        auto k2 = h * (J_inv * (grasp_vec - pre_grasp_vec + k1 / 2.0));
        auto k3 = h * (J_inv * (grasp_vec - pre_grasp_vec + k2 / 2.0));
        auto k4 = h * (J_inv * (grasp_vec - pre_grasp_vec + k3));
        auto del_theta = (k1 + 2 * k2 + 2 * k3 + k4) / 6.0;
        for (size_t j = 0; j < 6; ++j) {
            theta[j] += del_theta[j];
        }
        path.push_back(theta);
        // update the robot state
        robot_state->setJointGroupPositions(arm_name_, theta);
        robot_state->update();
        // check if the new state is in collision and out of bounds
        if (!robot_state->satisfiesBounds(joint_model_group)) {
            ROS_WARN("Out of bounds");
        }
        // check if the new state is in collision
        collision_detection::CollisionRequest collision_request;
        collision_detection::CollisionResult collision_result;
        collision_request.group_name = arm_name_;
        collision_result.clear();
//        robot_state->setFromDiffIK(robot_state->getJointModelGroup(arm_name_), J_inv * (grasp_vec - curr_grasp_vec));

        // update the current grasp vector using the current robot state FK
//        auto curr_pose = robot_state->getGlobalLinkTransform(robot_state->getLinkModel(joint_model_group->getLinkModelNames().back()));
//        curr_grasp_vec[0] = curr_pose.translation()[0]; curr_grasp_vec[1] = curr_pose.translation()[1]; curr_grasp_vec[2] = curr_pose.translation()[2];
//        auto euler = curr_pose.rotation().eulerAngles(2, 1, 0);
//        curr_grasp_vec[3] = euler[0]; curr_grasp_vec[4] = euler[1]; curr_grasp_vec[5] = euler[2];
//        ims::normalize_euler_zyx(curr_grasp_vec[5], curr_grasp_vec[4], curr_grasp_vec[3]);
        // update the Jacobian
        robot_state->getJacobian(joint_model_group, robot_state->getLinkModel(joint_model_group->getLinkModelNames().back()),
                                 Eigen::Vector3d(0, 0, 0), J);
    }
    // print path int degrees
    std::cout << "Path in degrees" << std::endl;
    for (auto & i : path) {
        for (size_t j = 0; j < 6; ++j) {
            i[j] = i[j] * 180.0 / M_PI;
            std::cout << i[j] << " ";
        }
        std::cout << std::endl;
    }

    return true;
}

bool ZeroTimePlanner::PlanAllPathsBetweenGoalRegions(std::vector<region>& goal_region1,
                                                     std::vector<region>& goal_region2) {
    InitMoveitOMPL();
    int counter {0};
    // Assuming that the goal regions are (x, y, z, r, p, y)
    for (size_t i {0} ; i < goal_region1.size(); ++i) {
        // Get the attractor state
        StateType attractor_state1 = goal_region1[i].path.back();
        // loop over all the goal states in the second goal region and plan a path to each one
        for (size_t j {0} ; j < goal_region2.size(); ++j) {
            // Get the goal state
            StateType attractor_state2 = goal_region2[j].path.back();
            // Plan a path from the two attractor states
            std::vector<StateType> path;
            if (!PlanPathBetweenTwoStatesOMPL(attractor_state1, attractor_state2, path)) {
                ROS_WARN("No path found");
                return false;
            } else {
                // Add the path to the list of paths
                goal_region1[i].paths.push_back(path);
                // Add the reversed path to the list of paths in the other goal region
                std::reverse(path.begin(), path.end());
                goal_region2[j].paths.push_back(path);
            }
            // check if counter divided by 100
            if (counter % 100 == 0) {
                std::cout << "counter: " << counter << std::endl;
            }
            counter++;
        }
    }
    return true;
}

bool ZeroTimePlanner::PlanPathBetweenTwoStatesOMPL(const StateType &start, const StateType &goal,
                                                   std::vector<StateType> &path) {
    ros::AsyncSpinner spinner(1);
    spinner.start();
    robot_state::RobotState start_state(*group_->getCurrentState());
    start_state.setJointGroupPositions(arm_name_, start);
    group_->setStartState(start_state);

    robot_state::RobotState goal_state(*group_->getCurrentState());
    goal_state.setJointGroupPositions(arm_name_, goal);    // "manipulator_1"
    group_->setJointValueTarget(goal_state);

    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
    auto ret = group_->plan(my_plan);
//    sleep(1); // Wanted to 0.1 but 'sleep' takes unsigned ints only. TODO: Check other sleep method

    if (ret != moveit_msgs::MoveItErrorCodes::SUCCESS) {
        ROS_WARN("OMPL failed to plan");
        return false;
    }
    else {
//        ROS_INFO("Solution found by OMPL using planner type: %s", group_->getPlannerId().c_str());
        // fill path
        path.resize(my_plan.trajectory_.joint_trajectory.points.size());
        for (size_t i = 0; i < my_plan.trajectory_.joint_trajectory.points.size(); ++i) {
            auto positions = my_plan.trajectory_.joint_trajectory.points[i].positions;
            path[i] = positions;
        }
        spinner.stop();
        return true;
    }
}










