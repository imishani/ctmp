/*
 * Copyright (C) 2023, Itamar Mishani
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Carnegie Mellon University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/*!
 * \file   ctmp_node_preprocess.cpp
 * \author Itamar Mishani (imishani@cmu.edu)
 * \date   4/20/23
*/

#include <ctmp/zero_time_planner.hpp>

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <moveit_msgs/DisplayTrajectory.h>



int main(int argc, char** argv) {

    ros::init(argc, argv, "ctmp_preprocess");
    ros::NodeHandle nh;
    ros::NodeHandle pnh("~");
    ros::AsyncSpinner spinner(8);
    spinner.start();

    // Before I will load it with launch file, lets set params to ROS server of the limits of the goal region:
    ros::param::set("/manipulator_1/regions/pick_region/min_limits",
                    std::vector<double>{-0.4, 0.55, 1.15, 0, 0, 0.0});
    ros::param::set("/manipulator_1/regions/pick_region/max_limits",
                    std::vector<double>{0.4, 1.0, 1.30, 0, 0, 0.0});
    ros::param::set("/manipulator_1/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 0.9, 0, 0, -0.1});
    ros::param::set("/manipulator_1/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.1, 0, 0, 0.1});

    ros::param::set("/ctmp_test/pick", true);
    ros::param::set("/manipulator_2/regions/pick_region/min_limits",
                    std::vector<double>{1.1, 0.5, 1.1 , 0.0, 0.0, 0.0});
    ros::param::set("/manipulator_2/regions/pick_region/max_limits",
                    std::vector<double>{1.4, -0.8, 1.25, 0.0, 0.0, 0.0});
    ros::param::set("/manipulator_2/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 0.9, 0.0, 0.0, 1.470796});
    ros::param::set("/manipulator_2/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.1, 0.0, 0.0, 1.570796});

    ros::param::set("/manipulator_3/regions/pick_region/min_limits",
                    std::vector<double>{-0.4, -1.0, 1.15, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/pick_region/max_limits",
                    std::vector<double>{0.4, -0.55, 1.30, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/place_region/min_limits",
                    std::vector<double>{0.4, -0.25, 0.9, 0, 0, 0.0});
    ros::param::set("/manipulator_3/regions/place_region/max_limits",
                    std::vector<double>{0.9, 0.25, 1.4, 0, 0, 0.0});


    std::string group_name = "manipulator_1";
    bool pick = true;
    if (argc == 0) {
        ROS_INFO_STREAM(BOLDMAGENTA << "No arguments given: using default values");
        ROS_INFO_STREAM("<group_name(string)>");
        ROS_INFO_STREAM("Using default values: manipulator_1 1" << RESET);
    } else if (argc == 2) {
        group_name = argv[1];
    } else if (argc == 3) {
        group_name = argv[1];
        pick = std::stoi(argv[2]);
    } else {
        ROS_INFO_STREAM(BOLDMAGENTA << "No arguments given: using default values");
        ROS_INFO_STREAM("<group_name(string)> <discretization(int)> <save_experience(bool int)>" );
        ROS_INFO_STREAM("Using default values: manipulator_1 1 0" << RESET);
    }
    ros::param::set("/" + group_name + "/pick", pick);


    // get manipulation_planning package path
    auto full_path = ros::package::getPath("manipulation_planning");
    std::string path_mprim = full_path + "/config/ws.mprim";

    // Define Robot interface to give commands and get info from moveit:
    moveit::planning_interface::MoveGroupInterface move_group(group_name);

    moveit::core::RobotStatePtr current_state = move_group.getCurrentState();

    // Test AStar in configuration space
    // @{
    auto* heuristic = new ims::SE3HeuristicRPY;
    ims::BestFirstSearchParams params(heuristic);

    auto df = ims::getDistanceFieldMoveIt();
    // show the bounding box of the distance field
    ros::Publisher bb_pub = nh.advertise<visualization_msgs::Marker>("bb_marker", 10);
    // get the planning frame
    ims::visualizeBoundingBox(df, bb_pub, move_group.getPlanningFrame());

    ims::MoveitInterface scene_interface (group_name);
    ims::ctmpActionType action_type(path_mprim);

    StateType discretization {0.02, 0.02, 0.02,
                              M_PI/180,
                              M_PI/180,
                              M_PI/180};
    action_type.Discretization(discretization);
    action_type.setSpaceType(ims::ManipulationType::SpaceType::WorkSpace); // Already the default

    std::shared_ptr<ims::ctmp_action_space> action_space = std::make_shared<ims::ctmp_action_space>(scene_interface,
                                                                                                    action_type);
    StateType start_state {0, 0, 0, 0, 0, 0};
    // get the current end effector pose
    // go to "ready" pose first
    move_group.setNamedTarget("ready" + group_name.substr(group_name.size() - 1));
    move_group.move();
    ros::Duration(1).sleep();

    geometry_msgs::PoseStamped current_pose = move_group.getCurrentPose();  // "arm_1tool0"

    start_state[0] = current_pose.pose.position.x;
    start_state[1] = current_pose.pose.position.y;
    start_state[2] = current_pose.pose.position.z;

    // If using hopf coordinates:
    Eigen::Quaterniond current_pose_eigen;
    tf::quaternionMsgToEigen(current_pose.pose.orientation, current_pose_eigen);

    ims::get_euler_zyx(current_pose_eigen, start_state[5], start_state[4], start_state[3]);
    ims::normalize_euler_zyx(start_state[5], start_state[4], start_state[3]);

//    StateType goal_state = {1.83, 0.956, 0.80 , 0.0, 0.0, 0.0};
//    StateType goal_state = {0.02, 0.76, 0.225, 0, -3.14519, 0.0};
    StateType goal_state = {-0.1, 0.8, 1.2, 0, 0, 0};
    // discrtize the goal state
    for (int i = 0; i < 6; i++) {
        goal_state[i] = std::round(goal_state[i] / discretization[i]) * discretization[i];
        start_state[i] = std::round(start_state[i] / discretization[i]) * discretization[i];
    }
    Eigen::Quaterniond start_pose_eigen;
    ims::from_euler_zyx(start_state[5], start_state[4], start_state[3], start_pose_eigen);
    geometry_msgs::Pose pose_check;
    pose_check.position.x = start_state[0]; pose_check.position.y = start_state[1]; pose_check.position.z = start_state[2];
    tf::quaternionEigenToMsg(start_pose_eigen, pose_check.orientation);


    std::cout << "rounded pose " << pose_check.position.x << " " << pose_check.position.y << " " << pose_check.position.z << " "
              << pose_check.orientation.x << " " << pose_check.orientation.y << " " << pose_check.orientation.z << " " << pose_check.orientation.w << std::endl;

    std::cout << "original pose " << current_pose.pose.position.x << " " << current_pose.pose.position.y << " " << current_pose.pose.position.z << " "
              << current_pose.pose.orientation.x << " " << current_pose.pose.orientation.y << " " << current_pose.pose.orientation.z << " " << current_pose.pose.orientation.w << std::endl;

    // check if the inverse kinematics solution exists for the current pose and check if the solution is equal to the current joint state
    std::vector<double> current_joint_state = move_group.getCurrentJointValues();
    std::vector<double> ik_solution;

    if (!scene_interface.calculateIK(pose_check, current_joint_state, ik_solution)) {
        std::cout << "No IK solution for the current pose" << std::endl;
        return 0;
    }
    else {
        ims::rad2deg(ik_solution); ims::rad2deg(current_joint_state);
        std::cout << "IK solution for the current pose" << std::endl;
        for (int i = 0; i < ik_solution.size(); i++) {
            std::cout << "joint " << i << " " << ik_solution[i] << " " << current_joint_state[i] << std::endl;
        }
    }
    std::cout << "start state " << start_state[0] << " " << start_state[1] << " " << start_state[2] << " "
              << start_state[3] << " " << start_state[4] << " " << start_state[5] << std::endl;
    std::cout << "goal state " << goal_state[0] << " " << goal_state[1] << " " << goal_state[2] << " "
              << goal_state[3] << " " << goal_state[4] << " " << goal_state[5] << std::endl;

    bool query_mode = false;
    ZeroTimePlanner planner;
    if (!planner.initializePlanner(action_space, params,
                                  start_state, goal_state,
                                   query_mode, "RRTConnect")){
        //raise error
        std::cout << "Planner initialization failed" << std::endl;
        return 0;
    }

    std::vector<StateType> path_;
    if (!planner.plan(path_, path_)) {
        if (query_mode)
            std::cout << "No path found" << std::endl;
        else
            std::cout << "\n" <<"Finished Preprocessing" << std::endl;
        return 0;
    }
    else {
        std::cout << "Path found" << std::endl;
    }


    // @}
    // Print nicely the path
    for (auto& state : path_) {
        for (auto& val : state) {
            std::cout << val << ", ";
        }
        std::cout << std::endl;
    }

    // {@
    /*
     * This part is just to test consecutive calls and plan between them
     * Delete it later
     */

    ros::param::set("/manipulator_1/pick", false);

    action_space = std::make_shared<ims::ctmp_action_space>(scene_interface,action_type);

//    goal_state = {0.4, 0.02, 0.1, 3.14519, 0.0, -3.14519/2.0};
    goal_state = {0.76, -0.1, 1.1, 0, 0, 0};
    // discrtize the goal state
    for (int i = 0; i < 6; i++) {
        goal_state[i] = std::round(goal_state[i] / discretization[i]) * discretization[i];
        start_state[i] = std::round(start_state[i] / discretization[i]) * discretization[i];
    }

    // check if the inverse kinematics solution exists for the current pose and check if the solution is equal to the current joint state
    query_mode = true;
    ZeroTimePlanner planner2;
    if (!planner2.initializePlanner(action_space, params,
                                   start_state, goal_state,
                                   query_mode, "RRTConnect")){
        //raise error
        std::cout << "Planner initialization failed" << std::endl;
        return 0;
    }

    std::vector<StateType> path_2;
    std::shared_ptr<std::vector<StateType>> path_ptr = std::make_shared<std::vector<StateType>>(path_);
    if (!planner2.plan(path_2, path_2, path_ptr)) {
        if (query_mode)
            std::cout << "No path found" << std::endl;
        else
            std::cout << "\n" <<"Finished Preprocessing" << std::endl;
        return 0;
    }
    else {
        std::cout << "Path found" << std::endl;
    }


    // @}



    // profile and execute the path
    // @{
    // execute in the joint space
    moveit_msgs::RobotTrajectory trajectory;
    // add current joint state to the path
    current_joint_state = move_group.getCurrentJointValues();
    move_group.setMaxVelocityScalingFactor(0.4);
    move_group.setMaxAccelerationScalingFactor(0.4);
    path_.insert(path_.begin(), current_joint_state);
    ims::profileTrajectory(path_.at(0),
                           path_.back(),
                           path_,
                           move_group,
                           trajectory);

    move_group.execute(trajectory);
    // Send the trajectory to joint_path_command topic to execute it
//    ros::Publisher joint_path_command_pub = nh.advertise<trajectory_msgs::JointTrajectory>("/joint_path_command", 10);
//    ros::Duration(1.0).sleep();
//    trajectory_msgs::JointTrajectory joint_trajectory;
//    joint_trajectory.joint_names = move_group.getVariableNames();
//    joint_trajectory.points.resize(trajectory.joint_trajectory.points.size());
//    for (int i = 0; i < trajectory.joint_trajectory.points.size(); i++) {
//        joint_trajectory.points[i].positions = trajectory.joint_trajectory.points[i].positions;
//        joint_trajectory.points[i].velocities = trajectory.joint_trajectory.points[i].velocities;
//        joint_trajectory.points[i].accelerations = trajectory.joint_trajectory.points[i].accelerations;
//        joint_trajectory.points[i].time_from_start = trajectory.joint_trajectory.points[i].time_from_start;
//    }
//
//    joint_path_command_pub.publish(joint_trajectory);

    ros::Duration(1.0).sleep();
//    move_group.setNamedTarget("ready");
//    move_group.move();
//    ros::Duration(1).sleep();


//     @}
//
//
//    std::vector<geometry_msgs::Pose> waypoints;
//    for (auto& state : path_) {
//        geometry_msgs::Pose pose;
//        pose.position.x = state[0];
//        pose.position.y = state[1];
//        pose.position.z = state[2];
//        Eigen::Quaterniond quat_res;
//        ims::from_euler_zyx(state[5], state[4], state[3], quat_res);
//        pose.orientation.x = quat_res.x(); pose.orientation.y = quat_res.y();
//        pose.orientation.z = quat_res.z(); pose.orientation.w = quat_res.w();
//        waypoints.push_back(pose);
//    }
//    moveit_msgs::RobotTrajectory trajectory;
//    double fraction = move_group.computeCartesianPath(waypoints, 0.01, 0.0, trajectory);
//    std::cout << "fraction: " << fraction << std::endl;
//    moveit::planning_interface::MoveGroupInterface::Plan my_plan;
//    my_plan.trajectory_ = trajectory;
//    move_group.execute(my_plan);
//
//     report stats
    moveit_msgs::RobotTrajectory trajectory2;
//    current_joint_state = move_group.getCurrentJointValues();
//    path_.insert(path_.begin(), current_joint_state);
    ims::profileTrajectory(path_2.at(0),
                           path_2.back(),
                           path_2,
                           move_group,
                           trajectory2);

    // show the trajectory in rviz
    if (false){
        moveit_msgs::DisplayTrajectory display_trajectory;
//    display_trajectory.trajectory_start = trajectory2.joint_trajectory.points.front();
        display_trajectory.trajectory.push_back(trajectory2);
        // get the current objects attached to the robot
        display_trajectory.model_id = move_group.getRobotModel()->getName();
        display_trajectory.trajectory_start.joint_state.name = move_group.getVariableNames();
        display_trajectory.trajectory_start.joint_state.position = trajectory2.joint_trajectory.points.front().positions;
        display_trajectory.trajectory_start.joint_state.velocity = trajectory2.joint_trajectory.points.front().velocities;
        display_trajectory.trajectory_start.joint_state.effort = trajectory2.joint_trajectory.points.front().accelerations;
        // get the planning_scene_interface
        moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
        auto obj = planning_scene_interface.getObjects();
        //make sure the object is attached to the robot
        for (auto& object : obj) {
            if (object.first == "part_c2") {
                moveit_msgs::AttachedCollisionObject attached_object;
                attached_object.link_name = "arm_1TCP";
                attached_object.object = object.second;
                display_trajectory.trajectory_start.attached_collision_objects.push_back(attached_object);
            }
        }

        ros::Publisher display_trajectory_publisher =
            nh.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
        display_trajectory_publisher.publish(display_trajectory);
    }

    move_group.execute(trajectory2);

    ros::Duration(1.0).sleep();
    move_group.setNamedTarget("ready1");
    move_group.move();
    ros::Duration(1).sleep();

    return 0;
}