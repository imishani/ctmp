# This file is a test for the LQR controller of a 2DOF manipulator
# We want to check what would be the profile of a system with A and B matrices equals to I and R and Q matrices equals to I as well.
# We should also add boundaries to the control input.
# We expect to see a system that is stable and converges to zero.

import numpy as np
import matplotlib.pyplot as plt

# Define the matrices
A = np.array([[1, 0], [0, 1]])
B = np.array([[1, 0], [0, 1]])
Q = np.array([[1, 0], [0, 1]])
R = np.array([[1, 0], [0, 1]])

u_bounds = np.array([[-1, 1], [-1, 1]])

def dre_solver(A, B, Q, R):
    """
    This function solves the DRE for the given matrices
    :param A: A matrix
    :param B: B matrix
    :param Q: Q matrix
    :param R: R matrix
    :return: the solution of the DRE
    """
    P, Pn = Q, Q
    for i in range(100):
        Pn = Q + np.matmul(np.matmul(A.T, P), A) - np.matmul(np.matmul(np.matmul(np.matmul(np.matmul(np.matmul(A.T, P), B), np.linalg.inv(R)), B.T), P), A)
        if np.linalg.norm(Pn - P) < 1e-5:
            break
        P = Pn
    return Pn


def lqr_controller(A, B, Q, R):
    """
    This function solves the DRE and calculates the K matrix
    :param A: A matrix
    :param B: B matrix
    :param Q: Q matrix
    :param R: R matrix
    :return: the K matrix
    """
    # consider the bounds
    P = dre_solver(A, B, Q, R)
    K = np.matmul(np.matmul(np.linalg.inv(R), B.T), P)
    return K


def simulate_system(A, B, K, x0, T):
    """
    This function simulates the system with the given matrices and initial state
    :param A: A matrix
    :param B: B matrix
    :param K: K matrix
    :param x0: initial state
    :param T: simulation time
    :return: the state and control history
    """
    x = np.zeros((2, T))
    u = np.zeros((2, T))
    x[:, 0] = x0[:, 0]
    for i in range(T - 1):
        u[:, i] = np.matmul(-K, x[:, i])
        x[:, i + 1] = np.matmul(A, x[:, i]) + np.matmul(B, u[:, i])
    return x, u



def plot_results(x, u):
    """
    This function plots the results of the simulation
    :param x: state history
    :param u: control history
    :return: None
    """
    plt.figure(1)
    plt.plot(x[0, :], label='x1')
    plt.plot(x[1, :], label='x2')
    plt.legend()
    plt.figure(2)
    plt.plot(u[0, :], label='u')
    plt.legend()
    plt.show()


if __name__ == "__main__":
    K = lqr_controller(A, B, Q, R)
    x0 = np.array([[8], [5]])
    T = 100
    x, u = simulate_system(A, B, K, x0, T)
    plot_results(x, u)
